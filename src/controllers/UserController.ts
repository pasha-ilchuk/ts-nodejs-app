import { Request, Response, Router } from 'express';
import User from '../models/User';

class UserController {
  public router: Router

  constructor() {
    this.router = Router();
    this.routes();
  }

  public all(req: Request, res: Response): void {
    User.find()
      .then((data) => {
        return res.status(200).json({ data });
      })
      .catch((error) => {
        res.status(500).json({ error });
        return error;
      });
  }

  public routes() {
    this.router.get('/', this.all);
  }
}

export default new UserController();