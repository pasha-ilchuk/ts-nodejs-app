import * as express from 'express';
import * as mongoose from 'mongoose';
import * as bodyParser from 'body-parser';
import * as logger from 'morgan';
// import * as helmet from 'helmet';
// import * as compression from 'compression';
// import * as cors from 'cors';

import UserController from './controllers/UserController';


class Server {
  public app: express.Application

  constructor() {
    this.app = express();
    this.config();
    this.routes();
  }

  config() {
    mongoose.connect('mongodb://localhost:27018/ts-fp-mongo', {
      useNewUrlParser: true,
    });

    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(bodyParser.json());
    this.app.use(logger('dev'));
    // this.app.use(compression());
    // this.app.use(helmet());
    // this.app.use(cors());
  }

  routes(): void {
    let router: express.Router
    router = express.Router();

    this.app.use('/', router);
    this.app.use('/api/v1/users', UserController.router);
  }
}

export default new Server().app;